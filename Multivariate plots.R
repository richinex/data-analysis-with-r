# Assessing multiple correlations at once
# Creating scatterplots in ggplot2
# Customizing them for maximum impact

library(ggplot2)

# Import data from EPA
URL <- 'https://raw.githubusercontent.com/fveronesi/PlottingToolbox/master/Sample_Data/EPA_Data_2014.csv'

epa <- read.csv(URL)

str(epa)

plot(epa[, c(5:9)])

plot(epa[, 5:9], col = epa$State)

qplot(SO2, NO2, data = epa, geom = 'point',
      xlab = 'SO2 (ppm)', ylab = 'NO2 (ppm)', col = State) +
  theme_classic()

# Now we try to subset the dataframe and remove iowa
epa_noIOWA <- epa[epa$State != 'Iowa',]

qplot(SO2, NO2, data = epa, geom = 'point',
      xlab = 'SO2 (ppm)', ylab = 'NO2 (ppm)', col = CO) +
  scale_color_gradientn(colours =
                         c('blue','light blue', 'green', 'red', 'orange'))
