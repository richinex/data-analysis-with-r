# Sometimes rasters are distributed as a collection of  adjacent tiles that cover small areas. if we want to create a complete raster for the whole study area, we need to create a mosaic.

# Downloading data from the USGS Earth Explorer
# Merging individual rasters
# Creating a single digital elevation model


# Load the raster package
library(raster)

# Unzip files
filename <- 'GMTED2010S10E000_300.zip'
unzip(paste0(getwd(), "/DTM_Data/", filename),
      exdir = paste0(getwd(),"/DTM_Data", sep = ''))

# Create a list with all the files in the directory
list_files <- list.files(path = './DTM_Data', pattern = '.tif', full.names = TRUE)



# Turn the list of files into a list of praster objects with lapply
list_rasters <- lapply(list_files, raster)

# Merge all the raster  objects into one. NB: This may be a time consuming step cos it creates a list of raster layers
DTM

DTM <- do.call('merge', list_rasters)

# Plot the raster mosaic
plot(DTM)

saveRDS(DTM, 'DTM.RDS')
