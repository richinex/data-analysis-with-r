# What is time-series: Data that has been measured at constant time intervals. the main characteristics of this data is that they all present some form of time wise variations. Most time series show variations that can be explained somehow.Finding explanations in the variation in time of the variable of interest  is the very first purpose of a time series analysis. Auto correlation is the correlation between data collected at certain time distances
# Purpose of time-series analysis
# Decomposition and correlation
# Forecasting: here the assume the time variation will continue in the future.
# in the mean method, all future values are assumed to be equal to the mean of the time series.
# In the naive method all future values are set equal to the last recorded value of the time series.
# The seasonal naive sets the value of future events equal to the average value for the same time interval of data e.g the average value of aguest will be used to forcast all future augusts.
# ARIMA: auto regressive integrated moving average
library(xts)

#Load the Crime dataset # https"//data.police.uk/data
Hammersmith <- read.csv("Crimes_Hammersmith_Robbery_2001_2015.csv")

View(Hammersmith)


#The standard format: ts
TS <- ts(Hammersmith$Crimes, 
         start=c(2011, 1), 
         end=c(2015, 12), 
         frequency=12)

print(TS)

plot(TS)


#Descriptive Statistics
mean.ts <- mean(TS)
sd.ts <- sd(TS)

plot(TS)
abline(h=mean.ts, col="red") # h represents horisontal line
abline(h=c(mean.ts+sd.ts, mean.ts-sd.ts), col="blue", lty=3)

#Stationarity
#https://people.duke.edu/~rnau/411diff.htm


#Monthly BoxPlot
boxplot(TS ~ cycle(TS))
abline(h=mean.ts, col="red")
abline(h=c(mean.ts+sd.ts, mean.ts-sd.ts), 
       col="blue", lty=3)



#Package XTS
#Read the columns DATETIME in the right format. recognizes missing values
Hammersmith$DATETIME <- as.POSIXct(Hammersmith$DATETIME, "GMT")

XTS <- xts(Hammersmith$Crimes, order.by=Hammersmith$DATETIME)


plot(XTS, major.format="%d-%m-%Y")

