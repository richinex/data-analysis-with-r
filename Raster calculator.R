# using simple algebra to calculate new raster values
# A bit of geomorphometry
# Advanced raster calculation: analytical relief shading

library(sp)
library(rgdal)
library(raster)


#Setting the working directory
setwd("E:/OneDrive/Packt - Data Analysis/Data")


#Nasa data from: https://eosweb.larc.nasa.gov/cgi-bin/sse/global.cgi
URL <- "https://eosweb.larc.nasa.gov/sse/global/text/22yr_T10M"

TempNASA <- read.table(file=URL, 
                       sep=" ", 
                       header=FALSE, 
                       skip=14)

January <- TempNASA[, 1:3]
names(January) <- c("Lat", "Lon", "Temp")

#Convert into a raster
TempRaster <- rasterFromXYZ(data.frame(X=January$Lon,
                                       Y=January$Lat,
                                       Z=January$Temp))


#Raster operations
#Convert Temperature from degrees celsius to fahrenheit
TempF <- (TempRaster*1.8) + 32

plot(TempF)

# To manually calculate the shaded relief of the DTM we downloaded in the previous sections. before we can do that we need to
#Calculate slope and aspect for the DTM we downloaded. We do this using the function terrain() whichg can be used for both

#Load the DTM we saved in Volume 1
DTM <- raster("DTM_combine.tif")

#Slope
slope <- terrain(DTM, opt="slope", unit="radians") # the first argument is the raster object, the second is the option slope and the third is the unit. A new raster with the slop is calculated

plot(slope)

# a similar syntax is used to calculate the relief
#Aspect
aspect <- terrain(DTM, opt="aspect", unit="radians")

plot(aspect)

# now we have all we need to calculate the shaded relief manually. From the resulting gdal output we can understand the lanscape very intuitively. The light source used for the relief is usually coming from the north west at an angle of 315 degrees which is the azimuth and an inclination of 45 degrees which is the zenith.

#Calculate Shaded Relief by hand
#http://pro.arcgis.com/en/pro-app/tool-reference/3d-analyst/how-hillshade-works.htm

# Since the azimuth and zenith remain constant through out the DTM, we first need to create two constant rasters (meaning rasters with the same values in all cells)
#Azimuth
azimuth <- raster(DTM) # first raster with the same resolution and bounding box of DTM.This is to make sure that the two rasters can be overlaid successfully
azimuth[] <- 315*(pi/180) # we assign the azimuth the value 315 degrees converted to radians which creates a constant raster
names(azimuth) <- "azimuth"


# we use trhe same process to create the zenith
#Zenith
zenith <- raster(DTM)
zenith[] <- 45*(pi/180)
names(zenith) <- "zenith"


# Now we proceed to hill shading. First we create a stack raster with all the data we need to solve the equation, then we create a new raster to put the shades of grey.
##Hillshading
staked <- stack(slope,aspect,azimuth,zenith)
hill.shd <- raster(aspect)
Shading <-
  ((cos(getValues(staked$zenith))*cos(getValues(staked$slope)))+
     (sin(getValues(staked$zenith))*sin(getValues(staked$slope))*
        cos(getValues(staked$azimuth) - getValues(staked$aspect))))
Shading[Shading < 0] <- 0
hill.shd[] <- 255*Shading # 255 is the number for the shades of grey for a grey colour scale


##Save Hillshaded relief
#Here the file is saved into an ASCII grid
writeRaster(hill.shd,filename="Shaded_Relief.tif",overwrite=T)
