

library(sp)
library(rgdal)
library(raster)




#Load the country boundary polygons from Natural Earth
NatEarth <- shapefile("Shapefile/ne_110m_admin_0_countries.shp")


#Set the URL with the CSV Files
URL <- "http://earthquake.usgs.gov/earthquakes/feed/v1.0/summary/all_day.csv"


#Loading CSV Files
USGS <- read.table(file=URL, 
                   sep=",", 
                   header=TRUE, 
                   na.string="")


#Transformation into a spatial object
coordinates(USGS)=~longitude+latitude

#Assign projection
projection(USGS)=CRS("+init=epsg:4326")


#Simple plot
plot(USGS)


#Change the symbol
#A list of all symbols for pch can be found here:
#http://www.endmemo.com/program/R/pchsymbols.php
plot(USGS, pch=20)


#We can also provide a custom symbol
#by copying and pasting it from the characters map in windows
plot(USGS, pch="+")


#Change color
plot(USGS, pch=20, col = "red")

#A full list of all the color names can be found here:
#http://www.stat.columbia.edu/~tzheng/files/Rcolor.pdf
plot(USGS, pch=20, col="slateblue4")


#Size of the marker
plot(USGS, pch=20, col="red", cex=2)



#Add borders
plot(USGS, pch=20, col="red", cex=1)
lines(NatEarth) # lines for polylines, and polygons, and points for spatial points




#Save the results in jpeg
help(jpeg)

jpeg(filename="Earthquake.jpg", width=4000, height=2000, 
     units="px", res=300)

plot(USGS, pch=20, col="red", cex=1)
lines(NatEarth)

dev.off()

############ USING NIGERIAN CONFLICTS DATA ########################
nigerian_conflicts <- conflict_data[, c('longitude', 'latitude', 'high')]
# assign names to the new data frame
names(nigerian_conflicts) <- c("Lon", "Lat", "deaths")
str(nigerian_conflicts)
nigerian_conflicts$Lon <- as.integer(nigerian_conflicts$Lon)
nigerian_conflicts$Lat <- as.integer(nigerian_conflicts$Lat)
nigerian_conflicts$deaths <- as.numeric(nigerian_conflicts$deaths)
coordinates(nigerian_conflicts) <-  ~Lon+Lat
#Assign projection
projection(nigerian_conflicts)=CRS("+init=epsg:4326")

#Save the results in jpeg

png(filename="nigerian_conflicts.jpg", width=940, height=740, 
     units="px", res=300)
plot(NatEarth)
plot(nigerian_conflicts, pch=20, col="red", cex=1)
lines(NatEarth)

dev.off()

plot(NatEarth)

z <- zoom(nigerian_conflicts, new = T)
lines(NatEarth)
plot(nigerian_conflicts)
plot(nigerian_conflicts, pch=20, col="red", cex=1)
lines(NatEarth)
